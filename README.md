# Wikipedia POC

# Problem:

Create a POC for Performing Search in Wikipedia. And Allow navigation between pages.

Solution:

Search for any available libraries already built on Wikipedia.

Search for Api’s available on Wikipedia.

Test Api if available

Available libraries:

Performed search on available libraries for wikipedia but did not found such other then source code of Wikipedia which is publically available.

# Api’s available.

Searched for wikipedia api and found link for Wikipedia Sandbox environment: https://en.wikipedia.org/wiki/Special:ApiSandbox

The above link gives access to the sandbox environment set for wikipedia api testing.

Searching from sandbox environment got the link of how to search wikipedia and get the list of search result link is given below:

https://en.wikipedia.org/w/api.php?format=json&action=query&titles=”<search>”

After spending more time for getting the detail for the search item found about the rest api link which is given below:

https://en.wikipedia.org/api/rest_v1/#/Mobile/

This link gives all the rest api for getting what is required to display in mobile application.

To get the description and detail of the search item there are 2 api which are required to use.

For getting the searched item major information use below given link:

https://en.wikipedia.org/api/rest_v1/page/mobile-sections-lead/<search_value>

For getting the detail / remaining information detail use below given link.

2.  https://en.wikipedia.org/api/rest_v1/page/mobile-sections-remaining/<search_value>



Using the above api mentioned now we can search on Home screen using wikipedia api and then can also see the details of the any item selected.


Note:  wikiclean and  wikiparserwithbliki library were added. but not used as there were a lot of problem in using them