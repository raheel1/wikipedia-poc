package com.iv.poc.wikipedia

import android.content.DialogInterface
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.google.gson.reflect.TypeToken
import com.iv.poc.wikipedia.models.searchedpages.SearchedPage
import com.iv.poc.wikipedia.recyclerviewadapters.WikiDataSectionAdapter
import com.iv.poc.wikipedia.recyclerviewadapters.WikiSearchListAdapter
import com.iv.poc.wikipedia.utils.ApiUrls
import com.koushikdutta.async.future.FutureCallback
import com.koushikdutta.ion.Ion
import kotlinx.android.synthetic.main.activity_main.*
import org.json.JSONArray
import org.json.JSONObject
import java.net.URLEncoder


class MainActivity : AppCompatActivity() {

    private lateinit var mAdapter: WikiSearchListAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        ivSearch.setOnClickListener {
            validateFields();
        }


        setUpListAdapter(ArrayList<SearchedPage>())

        showLoader(false)
    }

    private fun showLoader(showLoader: Boolean) {
        pbLoader.visibility = if (showLoader) View.VISIBLE else View.GONE
    }

    private fun validateFields() {
        if (TextUtils.isEmpty(etSearch.text.toString())) {
            etSearch.setError("Field is required");
            etSearch.requestFocus()
        } else {
            callApiToGetSearchDataFromWiki();
        }
    }

    private fun setUpListAdapter(list: ArrayList<SearchedPage>) {

        if (!::mAdapter.isInitialized && rvListOfWikiSearch.adapter == null) {
            rvListOfWikiSearch.layoutManager =
                LinearLayoutManager(this@MainActivity, LinearLayoutManager.VERTICAL, false)
            mAdapter = WikiSearchListAdapter(this@MainActivity, list)
            mAdapter.setClickListener(object : WikiSearchListAdapter.ItemClickListener {
                override fun onItemClick(view: View, position: Int, item: SearchedPage) {
                    showDialogForPages(item);
                }
            })
            rvListOfWikiSearch.adapter = mAdapter;
        } else {
            mAdapter.setListItem(list)
        }
    }

    private fun showDialogForPages(item: SearchedPage) {

        AlertDialog.Builder(this)
            .setPositiveButton("Open in Html Format",
                DialogInterface.OnClickListener { dialog, whichButton ->
                    dialog.dismiss()

                    WikiDisplayActivityFromApi.getOpenIntent(
                        this@MainActivity,
                        item.title,
                        WikiDataSectionAdapter.DataType.HtmlWeb
                    )

                    // Do something useful withe the position of the selected radio button
                })
            .setNegativeButton("Open in Text Format",
                DialogInterface.OnClickListener { dialog, whichButton ->
                    dialog.dismiss()
                    WikiDisplayActivityFromApi.getOpenIntent(
                        this@MainActivity,
                        item.title,
                        WikiDataSectionAdapter.DataType.Text
                    )

                    // Do something useful withe the position of the selected radio button
                })
            .show()

    }

    //api call


    private fun callApiToGetSearchDataFromWiki() {
        showLoader(true)
        val url = ApiUrls.searchAPi.replace("%1s#", URLEncoder.encode(etSearch.text.toString()))
        Ion.with(this)
            .load(url)
            .asString()
            .setCallback(FutureCallback<String> { e, result ->
                // do stuff with the result or error
                if (result == null || result == "null") {
                    showLoader(false)
                    // setBtnCheckForUpdateClickAble(true);
                    //if result is null.. it means data is no data is found
                } else {
                    try {
                        var response: JSONObject = JSONObject(result)
                        var queryObject: JSONObject = response.getJSONObject("query")
                        var pagesObject: JSONObject = queryObject.getJSONObject("pages")
                        var array: ArrayList<JSONObject> = ArrayList()
                        var keysList = pagesObject.keys()
                        while (keysList.hasNext()) {
                            var singleKey = keysList.next();
                            var singlePageItem: JSONObject = pagesObject.getJSONObject(singleKey)
                            array.add(singlePageItem)
                        }


//                        var array: JSONArray = JSONArray(result)
//                        array.get(0).toString()
//                        array.getJSONArray(1)
                        var arrayList: ArrayList<SearchedPage> =
                            Gson().fromJson(
                                array.toString(),
                                object : TypeToken<ArrayList<SearchedPage>>() {

                                }.type
                            )
                        setUpListAdapter(arrayList)

                        showLoader(false)
//array.getJSONArray(1)

                    } catch (e1: Exception) {
                        showLoader(false)
                        Toast.makeText(
                            this@MainActivity,
                            "Cannot find any search data",
                            Toast.LENGTH_LONG
                        ).show()
                        e1.printStackTrace()
                    }

                }
            })
    }
}
