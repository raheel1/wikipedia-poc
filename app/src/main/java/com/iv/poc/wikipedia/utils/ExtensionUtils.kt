package com.iv.poc.wikipedia.utils

fun String.replaceWikiDownloadUrl(): String {
    var text: String = "";
    if (this.contains("\"//upload.wikimedia.org")) {
        text = this.replace("//upload.wikimedia.org", "https://upload.wikimedia.org")
    }
    return text
}