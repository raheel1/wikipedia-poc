package com.iv.poc.wikipedia.utils

public class ApiUrls {
    companion object {
        val baseUrl: String = "https://en.wikipedia.org/w/api.php?"
        val baseUrlRestApi: String = "https://en.wikipedia.org/api/"
        //        var searchAPi: String = baseUrl + "action=opensearch&format=json&search="
        var searchAPi: String =
            baseUrl + "format=json&action=query&prop=description|pageimages|info&generator=prefixsearch&list=search&pithumbsize=320&gpssearch=%1s#&gpslimit=20&srsearch=%1s#"
        var getDataFromSearchItem: String =
            baseUrl + "format=json&action=query&prop=revisions&rvprop=content&titles="
        var getParseDataFromPageFromSearchItem: String =
            baseUrl + "format=json&action=parse&page="
        var getParsePageDirectly: String =
            "https://en.wikipedia.org/wiki/%1234#?action=raw"
        var getDataAgainSearchTextApi: String =
            baseUrlRestApi + "rest_v1/page/mobile-sections-lead/"
        var getDataARemainingSearchTextApi: String =
            baseUrlRestApi + "rest_v1/page/mobile-sections-remaining/"
    }
}