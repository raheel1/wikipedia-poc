package com.iv.poc.wikipedia.recyclerviewadapters

import android.content.Context
import android.text.Html
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebSettings
import android.webkit.WebView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.iv.poc.wikipedia.R
import com.iv.poc.wikipedia.models.searcheddata.SectionClass
import com.iv.poc.wikipedia.utils.replaceWikiDownloadUrl
import java.util.*


class WikiDataSectionAdapter(
    private var context: Context,
    private var mData: ArrayList<SectionClass>,
    private var mSectionData: ArrayList<SectionClass>,
    private var showDataType: DataType
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private val ITEM_TEXT: Int = 1;
    private val ITEM_SECTION: Int = 2;
    private val mInflater: LayoutInflater
    private var mClickListener: ItemClickListener? = null

    private val STYLE_TYPE_PAGE_CSS: String = "wikimedia-page-library.css"
    private val STYLE_TYPE_STYLE_CSS: String = "styles.css"

    enum class DataType {
        HtmlWeb, Text
    }


    init {
        this.mInflater = LayoutInflater.from(context)
    }

    override fun getItemViewType(position: Int): Int {
        return if (position == 0 && mData.get(position).id == 0 && TextUtils.isEmpty(
                mData.get(
                    position
                ).anchor
            )
        )
            ITEM_TEXT
        else
            ITEM_SECTION

    }

    // inflates the row layout from xml when needed
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if (viewType == ITEM_SECTION) {
            val view = mInflater.inflate(R.layout.item_data_section, parent, false)
            return ViewHolderSection(view)
        } else {
            val view = mInflater.inflate(R.layout.item_data_text, parent, false)
            return ViewHolderItem(view)

        }
    }

    // binds the data to the TextView in each row
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is ViewHolderItem) {
            val item = mData[position]

            holder.mWebViewDescription.loadDataWithBaseURL(
                "file:///android_asset/",
                "<link rel=\"stylesheet\" type=\"text/css\" href=\"" + STYLE_TYPE_PAGE_CSS + "\" />" + item.text,
                "text/html",
                "UTF-8",
                null
            );

            holder.mTitle.text = Html.fromHtml(item.text, Html.FROM_HTML_MODE_COMPACT)
            holder.mTitle.visibility = if (showDataType == DataType.Text) {
                View.VISIBLE
            } else {
                View.GONE
            }
            holder.mWebViewDescription.visibility = if (showDataType == DataType.HtmlWeb) {
                View.VISIBLE
            } else {
                View.GONE
            }
        } else if (holder is ViewHolderSection) {
            val item = mData[position]
            val sectionItem = mSectionData[position - 1]
            holder.mTitle.text = item.line
            holder.mDescription.text = Html.fromHtml(sectionItem.text, Html.FROM_HTML_MODE_COMPACT)
            holder.mWebViewDescription.loadDataWithBaseURL(
                "file:///android_asset/",
                "<link rel=\"stylesheet\" type=\"text/css\" href=\"" + STYLE_TYPE_PAGE_CSS + "\" />" + sectionItem.text,
                "text/html",
                "UTF-8",
                null
            );
            holder.mDescription.visibility = if (showDataType == DataType.Text) {
                View.VISIBLE
            } else {
                View.GONE
            }
            holder.mWebViewDescription.visibility = if (showDataType == DataType.HtmlWeb) {
                View.VISIBLE
            } else {
                View.GONE
            }
        }
    }

    // total number of rows
    override fun getItemCount(): Int {
        return mData.size
    }


    // stores and recycles views as they are scrolled off screen
    inner class ViewHolderSection internal constructor(itemView: View) :
        RecyclerView.ViewHolder(itemView),
        View.OnClickListener {
        internal var mTitle: TextView
        internal var mDescription: TextView
        internal var mWebViewDescription: WebView

        init {
            mTitle = itemView.findViewById(R.id.tvSectionName)
            mDescription = itemView.findViewById(R.id.tvSectionDescription)
            mWebViewDescription = itemView.findViewById(R.id.mvDescription)


            mWebViewDescription.getSettings().setLightTouchEnabled(true);
            mWebViewDescription.getSettings().setJavaScriptEnabled(true);
            mWebViewDescription.getSettings().setGeolocationEnabled(true);
            mWebViewDescription.setSoundEffectsEnabled(true);
            mWebViewDescription.getSettings()
                .setLayoutAlgorithm(WebSettings.LayoutAlgorithm.NORMAL);
            mWebViewDescription.getSettings().setUseWideViewPort(true);
            mWebViewDescription.scrollBarSize = 0
            mWebViewDescription.getSettings().setLoadWithOverviewMode(true);
            itemView.setOnClickListener(this)
        }

        override fun onClick(view: View) {
            if (mClickListener != null) mClickListener!!.onItemClick(
                view,
                adapterPosition,
                mData.get(adapterPosition)
            )
        }
    }

    // stores and recycles views as they are scrolled off screen
    inner class ViewHolderItem internal constructor(itemView: View) :
        RecyclerView.ViewHolder(itemView),
        View.OnClickListener {
        internal var mTitle: TextView
        internal var mWebViewDescription: WebView

        init {
            mTitle = itemView.findViewById(R.id.tvDescription)
            mWebViewDescription = itemView.findViewById(R.id.mvDescription)

            mWebViewDescription.getSettings().setLoadWithOverviewMode(true);
            mWebViewDescription.getSettings().setJavaScriptEnabled(true);
            mWebViewDescription.getSettings().setGeolocationEnabled(true);
            mWebViewDescription.setSoundEffectsEnabled(true);
            mWebViewDescription.getSettings()
                .setLayoutAlgorithm(WebSettings.LayoutAlgorithm.NORMAL);
            mWebViewDescription.getSettings().setUseWideViewPort(true);
            mWebViewDescription.scrollBarSize = 0
            itemView.setOnClickListener(this)
        }

        override fun onClick(view: View) {
            if (mClickListener != null) mClickListener!!.onItemClick(
                view,
                adapterPosition,
                mData.get(adapterPosition)
            )
        }
    }

    // convenience method for getting data at click position
    internal fun getItem(id: Int): SectionClass {
        return mData[id]
    }

    // allows clicks events to be caught
    internal fun setClickListener(itemClickListener: ItemClickListener) {
        this.mClickListener = itemClickListener
    }

    fun addItems(list: ArrayList<SectionClass>) {
        mData.addAll(list)
        notifyDataSetChanged()
    }

    fun setListItem(list: ArrayList<SectionClass>) {
        mData = list
        notifyDataSetChanged()
    }

    // parent activity will implement this method to respond to click events
    interface ItemClickListener {
        fun onItemClick(view: View, position: Int, item: SectionClass)
    }
}