package com.iv.poc.wikipedia

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.iv.poc.wikipedia.models.searcheddata.SectionClass
import com.iv.poc.wikipedia.recyclerviewadapters.WikiDataSectionAdapter
import com.iv.poc.wikipedia.utils.ApiUrls
import com.koushikdutta.async.future.FutureCallback
import com.koushikdutta.ion.Ion
import kotlinx.android.synthetic.main.activity_wiki_display_from_api.*
import org.json.JSONObject
import java.net.URLEncoder


class WikiDisplayActivityFromApi : AppCompatActivity() {

    private lateinit var showPageType: WikiDataSectionAdapter.DataType
    private lateinit var mAdapter: WikiDataSectionAdapter
    private var searchText: String = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_wiki_display_from_api)

        searchText = intent.getStringExtra(WikiDisplayActivityFromApi.KEY_SEARCH_ITEM)
        showPageType =
            intent.getSerializableExtra(WikiDisplayActivityFromApi.KEY_SHOW_PAGE_TYPE) as WikiDataSectionAdapter.DataType

        callApiToGetSearchDataFromWiki()
//        setUpListAdapter(ArrayList<SectionClass>(), ArrayList<SectionClass>())

        showLoader(false)
    }

    private fun showLoader(showLoader: Boolean) {
        pbLoader.visibility = if (showLoader) View.VISIBLE else View.GONE
    }

    private fun setUpListAdapter(
        list: ArrayList<SectionClass>,
        subDetails: ArrayList<SectionClass>
    ) {

        if (!::mAdapter.isInitialized && rvListOfData.adapter == null) {
            rvListOfData.layoutManager =
                LinearLayoutManager(
                    this@WikiDisplayActivityFromApi,
                    LinearLayoutManager.VERTICAL,
                    false
                )
            mAdapter = WikiDataSectionAdapter(
                this@WikiDisplayActivityFromApi,
                list,
                subDetails,
                showPageType
//                WikiDataSectionAdapter.DataType.Text
            )
            mAdapter.setClickListener(object : WikiDataSectionAdapter.ItemClickListener {
                override fun onItemClick(view: View, position: Int, item: SectionClass) {
//                    showDialogForPages(item);
//                    WikiParseDisplayActivity.getOpenIntent(
//                        this@WikiDisplayActivityFromApi,
//                        item.text
//                    )
                }
            })
            rvListOfData.adapter = mAdapter;
        } else {
            mAdapter.setListItem(list)
        }
    }


    //api call


    private fun callApiToGetSearchDataFromWiki() {
        showLoader(true)
        val url = ApiUrls.getDataAgainSearchTextApi + URLEncoder.encode(searchText)
        Ion.with(this)
            .load(url)
            .asString()
            .setCallback(FutureCallback<String> { e, result ->
                // do stuff with the result or error
                if (result == null || result == "null") {
                    showLoader(false)
                    // setBtnCheckForUpdateClickAble(true);
                    //if result is null.. it means data is no data is found
                } else {
                    try {
                        var response: JSONObject = JSONObject(result)
                        var displayTitle = response.getString("displaytitle");
                        tvTitle.text = displayTitle;
                        var description = response.getString("description");
                        tvDescription.text = description
                        var arrayHatNotes = response.getJSONArray("hatnotes")
                        var hatNotes = ""

                        var i = 0;
                        while (arrayHatNotes != null && arrayHatNotes.length() > 0 && i < arrayHatNotes.length()) {
                            hatNotes += arrayHatNotes.getString(i) + "\n"
                            i++
                        }


                        var arrayOfSections = response.getJSONArray("sections")

//                        var queryObject: JSONObject = response.getJSONObject("query")
//                        var pagesObject: JSONObject = queryObject.getJSONObject("pages")
//                        var array: ArrayList<JSONObject> = ArrayList()
//                        var keysList = pagesObject.keys()
//                        while (keysList.hasNext()) {
//                            var singleKey = keysList.next();
//                            var singlePageItem: JSONObject = pagesObject.getJSONObject(singleKey)
//                            array.add(singlePageItem)
//                        }


//                        var array: JSONArray = JSONArray(result)
//                        array.get(0).toString()
//                        array.getJSONArray(1)
                        var arrayList: ArrayList<SectionClass> = //ArrayList()
                            Gson().fromJson(
                                arrayOfSections.toString(),
                                object : TypeToken<ArrayList<SectionClass>>() {

                                }.type
                            )
                        callApiToGetRemainingSectionFromWiki(arrayList)


//array.getJSONArray(1)

                    } catch (e1: Exception) {
                        showLoader(false)
                        Toast.makeText(
                            this@WikiDisplayActivityFromApi,
                            "Cannot find any search data",
                            Toast.LENGTH_LONG
                        ).show()
                        e1.printStackTrace()
                    }

                }
            })
    }

    private fun callApiToGetRemainingSectionFromWiki(list: ArrayList<SectionClass>) {
        showLoader(true)
        val url = ApiUrls.getDataARemainingSearchTextApi + searchText.replace(" ", "_");
        Ion.with(this)
            .load(url)
            .asString()
            .setCallback(FutureCallback<String> { e, result ->
                // do stuff with the result or error
                if (result == null || result == "null") {
                    showLoader(false)
                    // setBtnCheckForUpdateClickAble(true);
                    //if result is null.. it means data is no data is found
                } else {
                    try {
                        var response: JSONObject = JSONObject(result)
                        var arrayOfSections = response.getJSONArray("sections")

                        var arrayList: ArrayList<SectionClass> = //ArrayList()
                            Gson().fromJson(
                                arrayOfSections.toString(),
                                object : TypeToken<ArrayList<SectionClass>>() {

                                }.type
                            )
                        setUpListAdapter(list, arrayList)
                        showLoader(false)

                    } catch (e1: Exception) {
                        showLoader(false)
                        Toast.makeText(
                            this@WikiDisplayActivityFromApi,
                            "Cannot find any search data",
                            Toast.LENGTH_LONG
                        ).show()
                        e1.printStackTrace()
                    }

                }
            })
    }

    companion object {
        val KEY_SEARCH_ITEM: String = "search_item"
        val KEY_SHOW_PAGE_TYPE: String = "pageType"

        public fun getOpenIntent(
            mContext: Context,
            item: String,
            showInBooleanFormate: WikiDataSectionAdapter.DataType
        ) {
            var mIntent: Intent = Intent(mContext, WikiDisplayActivityFromApi::class.java)
            mIntent.putExtra(KEY_SEARCH_ITEM, item)
            mIntent.putExtra(KEY_SHOW_PAGE_TYPE, showInBooleanFormate)
            mContext.startActivity(mIntent)
        }
    }
}
