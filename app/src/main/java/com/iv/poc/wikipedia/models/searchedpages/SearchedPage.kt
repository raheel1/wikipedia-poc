package com.iv.poc.wikipedia.models.searchedpages

import java.io.Serializable

public class SearchedPage : Serializable {
    var pageid: Int = 0
    var ns = 0
    var title = ""
    var index = 1
    var description = ""
    var descriptionsource = ""
    var thumbnail: ImageModel? = null
    var pageimage = ""
    var contentmodel = ""
    var pagelanguage = ""
    var pagelanguagehtmlcode = ""
    var pagelanguagedir = ""
    var touched = ""
    var lastrevid = 0
    var length = 0
}