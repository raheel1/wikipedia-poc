package com.iv.poc.wikipedia.models.searcheddata

import android.text.TextUtils
import com.iv.poc.wikipedia.utils.replaceWikiDownloadUrl
import java.io.Serializable

public class SectionClass : Serializable {
    var id: Int = 0;
    var text: String = ""
        get() {
            if (TextUtils.isEmpty(field)) {
                return ""
            }
            return field.replaceWikiDownloadUrl()
        }
    var toclevel: Int = 0;
    var anchor: String = ""
    var line: String = ""

}