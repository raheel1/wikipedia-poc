package com.iv.poc.wikipedia.recyclerviewadapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.iv.poc.wikipedia.R
import com.iv.poc.wikipedia.models.searchedpages.SearchedPage
import java.util.ArrayList


class WikiSearchListAdapter(
    private var context: Context,
    private var mData: ArrayList<SearchedPage>
) :
    RecyclerView.Adapter<WikiSearchListAdapter.ViewHolder>() {
    private val mInflater: LayoutInflater
    private var mClickListener: ItemClickListener? = null

    init {
        this.mInflater = LayoutInflater.from(context)
    }

    // inflates the row layout from xml when needed
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = mInflater.inflate(R.layout.item_search_wiki_list, parent, false)
        return ViewHolder(view)
    }

    // binds the data to the TextView in each row
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = mData[position]
        holder.mTitle.text = item.title
        holder.mDescription.text = item.description

        Glide.with(context).load(
            if (item.thumbnail == null)
                ""
            else item.thumbnail!!.source
        ).into(holder.mThumbnail)
    }

    // total number of rows
    override fun getItemCount(): Int {
        return mData.size
    }


    // stores and recycles views as they are scrolled off screen
    inner class ViewHolder internal constructor(itemView: View) : RecyclerView.ViewHolder(itemView),
        View.OnClickListener {
        internal var mTitle: TextView
        internal var mDescription: TextView
        internal var mThumbnail: ImageView

        init {
            mTitle = itemView.findViewById(R.id.tvSearchTitle)
            mDescription = itemView.findViewById(R.id.tvSearchDescription)
            mThumbnail = itemView.findViewById(R.id.ivThumbnail)
            itemView.setOnClickListener(this)
        }

        override fun onClick(view: View) {
            if (mClickListener != null) mClickListener!!.onItemClick(
                view,
                adapterPosition,
                mData.get(adapterPosition)
            )
        }
    }

    // convenience method for getting data at click position
    internal fun getItem(id: Int): SearchedPage {
        return mData[id]
    }

    // allows clicks events to be caught
    internal fun setClickListener(itemClickListener: ItemClickListener) {
        this.mClickListener = itemClickListener
    }

    fun addItems(list: ArrayList<SearchedPage>) {
        mData.addAll(list)
        notifyDataSetChanged()
    }

    fun setListItem(list: ArrayList<SearchedPage>) {
        mData = list
        notifyDataSetChanged()
    }

    // parent activity will implement this method to respond to click events
    interface ItemClickListener {
        fun onItemClick(view: View, position: Int, item: SearchedPage)
    }
}