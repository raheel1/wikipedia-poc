package com.iv.poc.wikiparserwithbliki.html.jspwiki;

import com.iv.poc.wikiparserwithbliki.html.wikipedia.ConvertEmptyHTMLTag;
import com.iv.poc.wikiparserwithbliki.htmlcleaner.TagNode;



public class TdJSPWikiTag extends ConvertEmptyHTMLTag {

	@Override
	public void open(TagNode node, StringBuilder resultBuffer) {
		resultBuffer.append("|");
	}

}
