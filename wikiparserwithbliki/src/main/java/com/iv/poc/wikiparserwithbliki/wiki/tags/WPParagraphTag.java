package com.iv.poc.wikiparserwithbliki.wiki.tags;


public class WPParagraphTag extends WPTag {
	public WPParagraphTag() {
		super("p");
	}

	@Override
	public boolean isReduceTokenStack() {
		return true;
	}

}