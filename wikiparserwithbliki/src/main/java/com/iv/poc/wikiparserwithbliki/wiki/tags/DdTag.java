package com.iv.poc.wikiparserwithbliki.wiki.tags;


public class DdTag extends HTMLBlockTag {
	public DdTag() {
		super("dd", "|dl|");
	}
}