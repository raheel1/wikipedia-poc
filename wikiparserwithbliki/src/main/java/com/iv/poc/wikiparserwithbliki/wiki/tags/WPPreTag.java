package com.iv.poc.wikiparserwithbliki.wiki.tags;

import com.iv.poc.wikiparserwithbliki.wiki.filter.ITextConverter;
import com.iv.poc.wikiparserwithbliki.wiki.model.IWikiModel;

import java.io.IOException;

/**
 * A wiki block starting with a space on each line, rendered with the HTML
 * <code>pre</code> Tag
 * 
 * @see PreTag
 */
public class WPPreTag extends HTMLBlockTag {
	public WPPreTag() {
		super("pre", null);
	}

	@Override
	public void renderHTML(ITextConverter converter, Appendable buf, IWikiModel model) throws IOException {
		super.renderHTML(converter, buf, model);
	}
}
