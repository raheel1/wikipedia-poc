package com.iv.poc.wikiparserwithbliki.wiki.tags.util;


/**
 * Interface that indicates, that a wiki tags content (i.e. body) 
 * shouldn't be parsed for other wiki tags
 * 
 * @see com.iv.poc.wikiparserwithbliki.wiki.filter.api.WikipediaParser
 */
public interface INoBodyParsingTag extends IBodyTag {

}
