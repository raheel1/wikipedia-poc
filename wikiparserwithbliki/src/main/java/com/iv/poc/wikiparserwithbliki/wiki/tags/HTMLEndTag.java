package com.iv.poc.wikiparserwithbliki.wiki.tags;

import com.iv.poc.wikiparserwithbliki.htmlcleaner.EndTagToken;


public class HTMLEndTag extends EndTagToken 
{

	public HTMLEndTag(String name)
	{
		super(name);
	}


	@Override
	public boolean isReduceTokenStack()
	{
		return false;
	}
	
}