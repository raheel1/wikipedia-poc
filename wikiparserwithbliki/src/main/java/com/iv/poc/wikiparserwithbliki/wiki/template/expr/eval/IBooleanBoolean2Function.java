package com.iv.poc.wikiparserwithbliki.wiki.template.expr.eval;


public interface IBooleanBoolean2Function {
  public boolean evaluate(boolean arg1, boolean arg2);
}
