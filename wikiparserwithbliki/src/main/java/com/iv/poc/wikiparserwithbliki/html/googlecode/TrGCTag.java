package com.iv.poc.wikiparserwithbliki.html.googlecode;

import com.iv.poc.wikiparserwithbliki.html.wikipedia.ConvertEmptyHTMLTag;
import com.iv.poc.wikiparserwithbliki.htmlcleaner.TagNode;



public class TrGCTag extends ConvertEmptyHTMLTag {

	@Override
	public void open(TagNode node, StringBuilder resultBuffer) {
		resultBuffer.append("\n|");
	}

	@Override
	public void close(TagNode node, StringBuilder resultBuffer) {
		resultBuffer.append("|");
	}

}
