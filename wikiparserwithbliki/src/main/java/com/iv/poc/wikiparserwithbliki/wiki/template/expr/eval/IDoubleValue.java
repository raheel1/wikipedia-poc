package com.iv.poc.wikiparserwithbliki.wiki.template.expr.eval;

public interface IDoubleValue {

	public abstract double getValue();

	public abstract void setValue(double value);

}