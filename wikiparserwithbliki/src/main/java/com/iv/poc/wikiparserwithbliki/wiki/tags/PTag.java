package com.iv.poc.wikiparserwithbliki.wiki.tags;

import com.iv.poc.wikiparserwithbliki.wiki.filter.ITextConverter;
import com.iv.poc.wikiparserwithbliki.wiki.model.Configuration;
import com.iv.poc.wikiparserwithbliki.wiki.model.IWikiModel;

import java.io.IOException;
import java.util.List;

public class PTag extends HTMLBlockTag {
	public PTag() {
		super("p", "|address" + Configuration.SPECIAL_BLOCK_TAGS);
	}

	@Override
	public String getCloseTag() {
		return "\n</p>";
	}

	@Override
	public void renderHTML(ITextConverter converter, Appendable buf, IWikiModel model) throws IOException {
		// super.renderHTML(converter, buf, model);
		// use this to avoid empty <p /> tags in the html
		List<Object> children = this.getChildren();
		if (children.size() != 0) {
			super.renderHTML(converter, buf, model);
		}
	}
}