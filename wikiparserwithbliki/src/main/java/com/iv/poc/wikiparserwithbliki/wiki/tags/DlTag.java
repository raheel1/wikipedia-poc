package com.iv.poc.wikiparserwithbliki.wiki.tags;

import com.iv.poc.wikiparserwithbliki.wiki.model.Configuration;


public class DlTag extends HTMLBlockTag {
	public DlTag() {
		super("dl", Configuration.SPECIAL_BLOCK_TAGS);
	}
}