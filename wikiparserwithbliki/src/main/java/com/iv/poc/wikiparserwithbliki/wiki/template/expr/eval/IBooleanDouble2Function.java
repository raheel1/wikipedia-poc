package com.iv.poc.wikiparserwithbliki.wiki.template.expr.eval;


public interface IBooleanDouble2Function {
  public boolean evaluate(double arg1, double arg2);
}
