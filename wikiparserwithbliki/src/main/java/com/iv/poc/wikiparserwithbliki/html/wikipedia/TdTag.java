package com.iv.poc.wikiparserwithbliki.html.wikipedia;

import com.iv.poc.wikiparserwithbliki.htmlcleaner.TagNode;


public class TdTag extends ConvertEmptyHTMLTag {

	@Override
	public void open(TagNode node, StringBuilder resultBuffer) {
		resultBuffer.append("\n|");
	}

}
