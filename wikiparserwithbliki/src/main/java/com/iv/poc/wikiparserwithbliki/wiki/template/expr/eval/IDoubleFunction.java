package com.iv.poc.wikiparserwithbliki.wiki.template.expr.eval;

import com.iv.poc.wikiparserwithbliki.wiki.template.expr.ast.FunctionNode;

public interface IDoubleFunction {
  public double evaluate(DoubleEvaluator engine, FunctionNode function);
}
